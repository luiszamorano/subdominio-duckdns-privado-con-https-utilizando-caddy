# LEEME

Notar que, dentro del repositorio existe un archivo config.env donde deberás detallar:

- **TOKEN:** token que proporciona tu cuenta de duckdns
- **SUBDOMAINS:** subdominio asociado en duckdns (sin el .duckdns.org)
- **DOMAIN:** dominio asociado en duckdns (ahora sí con .duckdns.org)
- **REVERSE_IP:** ip donde estará tu servicio al que quieres que se redireccione, de manera segura, al ingresar a tu domninio duck.